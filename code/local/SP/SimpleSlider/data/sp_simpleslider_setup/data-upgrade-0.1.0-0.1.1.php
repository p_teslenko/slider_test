<?php

$collection = [
    [
        'title' => 'title1',
        'alt' => 'alt1',
        'image' => 'image1',
        'text' => 'Free Text 1',
        'short_description' => 'short description 1',
        'display_to' => '2017-08-28',
        'display_from' => '2017-08-19',
    ],
    [
        'title' => 'title2',
        'alt' => 'alt2',
        'image' => 'image2',
        'text' => 'Free Text 2',
        'short_description' => 'short description 2',
        'display_to' => '2017-08-28',
        'display_from' => '2017-08-19',
    ],
    [
        'title' => 'title3',
        'alt' => 'alt3',
        'image' => 'image3',
        'text' => 'Free Text 3',
        'short_description' => 'short description 3',
        'display_to' => '2017-07-28',
        'display_from' => '2017-07-19',
    ],
    [
        'title' => 'title4',
        'alt' => 'alt4',
        'image' => 'image4',
        'text' => 'Free Text 4',
        'short_description' => 'short description 4',
        'display_to' => '2017-08-28',
        'display_from' => '2017-08-19',
    ],
    [
        'title' => 'title1',
        'alt' => 'alt1',
        'image' => 'image1',
        'text' => 'Free Text 1',
        'short_description' => 'short description 1',
        'display_to' => '2017-08-28',
        'display_from' => '2017-08-19',
    ],
];

foreach ($collection as $obj) {
    try {
        $model = Mage::getModel('sp_simpleslider/slider');
        $displayTo = Mage::getModel('core/date')->timestamp(strtotime($obj['display_to']));
        $displayTo = date('d.m.Y', $displayTo);
        $displayFrom = Mage::getModel('core/date')->timestamp(strtotime($obj['display_from']));
        $displayFrom = date('d.m.Y', $displayFrom);
        $model->setData([
            'title' => $obj['title'],
            'alt'   => $obj['alt'],
            'image' => $obj['image'],
            'text'  => $obj['text'],
            'short_description' => $obj['short_description'],
            'display_to' => $displayTo,
            'display_from' => $displayFrom
        ]);
        $model->save();
    } catch (\Exception $e) {
        Mage::logException($e);
    }
}
