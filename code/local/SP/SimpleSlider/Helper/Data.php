<?php
class SP_SimpleSlider_Helper_Data extends Mage_Core_Helper_Abstract
{
  public function savePostData(array $data)
    {
        if (is_array($data['image']) && isset($data['image']['value'])) {
            if (empty($data['image']['delete'])) {
                $data['image'] = $data['image']['value'];
            } else {
                $this->deleteAllSliderImages($data['image']['value']);
                $data['image'] = '';
            }
        }
        if ($imageName = $this->uploadFile()) {
            $data['image'] = $imageName;
        }
        $model = Mage::getModel('sp_simpleslider/slider');
        $model->setData($data);
        $model->save();
    }

    /**
     * @return string|bool
     */
    public function uploadFile()
    {
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader('image');
                $uploader->setAllowedExtensions($this->_allowedExtensions);
                $uploader->setAllowRenameFiles(true);
                $uploader->setAllowCreateFolders(true);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'sp_simpleslider' . DS;
                $imageName = preg_replace('/\s+/', '_', $_FILES['image']['name']);
                $uploader->save($path, $imageName);
                return $imageName;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError($this->__('Image upload failed'));
            }
        }
        return false;
    }

}
