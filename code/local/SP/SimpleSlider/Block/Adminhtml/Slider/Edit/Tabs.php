<?php
class SP_Simpleslider_Block_Adminhtml_Slider_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * SP_Events_Block_Adminhtml_Slider_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('slider_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('sp_simpleslider')->__('Slider Information'));
    }
    /**
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        $this->addTab('general',
            [
                'label'     => Mage::helper('sp_simpleslider')->__('General'),
                'title'     => Mage::helper('sp_simpleslider')->__('General'),
                'content'   => $this->getLayout()
                    ->createBlock('simpleslider/adminhtml_slider_edit_tabs_general')
                    ->toHtml(),
            ]
        );
        $this->addTab('image',
            [
                'label'     => Mage::helper('sp_simpleslider')->__('Image'),
                'title'     => Mage::helper('sp_simpleslider')->__('Image'),
                'content'   => $this->getLayout()
                    ->createBlock('simpleslider/adminhtml_slider_edit_tabs_image')
                    ->toHtml(),
            ]
        );
        return parent::_beforeToHtml();
    }
}