<?php
class SP_Simpleslider_Block_Adminhtml_Slider_Edit_Tabs_General
    extends Mage_Adminhtml_Block_Widget_Form

{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'slider_form',
            array(
                'legend' => Mage::helper('sp_simpleslider')->__('Slide information')
            )
        );
        $model = Mage::registry(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY);
        if ($model && $model->getId()) {
            $fieldset->addField(
                'slider_id',
                'hidden',
                [
                    'name'      => 'slider_id',
                    'required'  => true
                ]
            );
        }
        $fieldset->addField(
            'title',
            'text',
            [
                'label' => Mage::helper('sp_simpleslider')->__('Title'),
                'required' => true,
                'name' => 'title',
            ]
        );
        $fieldset->addField(
            'text',
            'text',
            [
                'label' => Mage::helper('sp_simpleslider')->__('Text'),
                'required' => false,
                'name' => 'text',
            ]
        );
        $fieldset->addField(
            'display_from',
            'date',
            [
                'name'               => 'display_from',
                'label'              => Mage::helper('sp_simpleslider')->__('Date From'),
                'tabindex'           => 1,
                'image'              => $this->getSkinUrl('images/grid-cal.gif'),
                'format'             => Mage::app()->getLocale()
                    ->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            ]
        );
        $fieldset->addField(
            'display_to',
            'date',
            [
                'name'               => 'display_to',
                'label'              => Mage::helper('sp_simpleslider')->__('Date To'),
                'tabindex'           => 1,
                'image'              => $this->getSkinUrl('images/grid-cal.gif'),
                'format'             => Mage::app()->getLocale()
                    ->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            ]
        );
        $fieldset->addField(
            'is_active',
            'select',
            array(
                'label' => Mage::helper('sp_simpleslider')->__('Status'),
                'name' => 'is_active',
                'values' => array(
                    [
                        'value' => 1,
                        'label' => Mage::helper('sp_simpleslider')->__('Enabled'),
                    ],
                    [
                        'value' => 0,
                        'label' => Mage::helper('sp_simpleslider')->__('Disabled'),
                    ],
                ),
            )
        );
        if (Mage::getSingleton('adminhtml/session')->getData(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY)) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getData(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY));
            Mage::getSingleton('adminhtml/session')->setData('event_data', null);
        } elseif (Mage::registry(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY)) {
            $form->setValues(Mage::registry(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY)->getData());
        }
        return parent::_prepareForm();
    }
}