<?php
class SP_Simpleslider_Block_Adminhtml_Slider_Edit_Tabs_Image
    extends Mage_Adminhtml_Block_Widget_Form

{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'slider_image',
            array(
                'legend' => Mage::helper('sp_simpleslider')->__('Image')
            )
        );
        //$fieldset->addType('event_image', 'SP_Events_Block_Adminhtml_Renderer_Form_Image');
        $fieldset->addField(
            'image',
            'image',
            [
                'label' => Mage::helper('sp_simpleslider')->__('Image'),
                'required' => true,
                'name' => 'image',
            ]
        );
        $fieldset->addField(
            'alt',
            'text',
            [
                'label' => Mage::helper('sp_simpleslider')->__('Alt'),
                'required' => true,
                'name' => 'alt',
            ]
        );
        if (Mage::getSingleton('adminhtml/session')->getData(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY)) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getData(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY));
            Mage::getSingleton('adminhtml/session')->setData('event_data', null);
        } elseif (Mage::registry(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY)) {
            $form->setValues(Mage::registry(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY)->getData());
        }
        return parent::_prepareForm();
    }
}