<?php
class SP_SimpleSlider_Block_Adminhtml_Slider_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sliderGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);

    }
    protected function _prepareCollection()
    {
        $collection = Mage::getSingleton('sp_simpleslider/slider')
              ->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'slider_id',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('ID'),
                'index' => 'slider_id',
            ));
        $this->addColumn(
            'title',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('Title'),
                'index' => 'title',
            ));
        $this->addColumn(
            'alt',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('Alt'),
                'index' => 'alt',
            ));
        $this->addColumn(
            'display_from',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('Display from'),
                'index' => 'display_from',
            ));
        $this->addColumn(
            'display_to',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('Display to'),
                'index' => 'display_to',
            ));
        $this->addColumn(
            'is_active',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('Is active'),
                'index' => 'is_active',
                'type' => 'options',
                'options' => array(
                    1 => Mage::helper('sp_simpleslider')->__('Enabled'),
                    0 => Mage::helper('sp_simpleslider')->__('Disaibled'),
                ),
            ));
        $link = array(
            'base'      => '*/*/edit' .
                '/id/' . $this->getRequest()->getParam('id') .
                '/action/' . 'delete');
        $this->addColumn(
            'action',
            array(
                'header'=> Mage::helper('sp_simpleslider')->__('Action'),
                'index' => 'action',
                'type' => 'action',
                'width' => '100',
                'getter' => 'getId',
                'actions' => array(
                      array(
                          'caption' => Mage::helper('sp_simpleslider')->__('Edit'),
                          'url' => array('base' => '*/*/edit'),
                          'field'   => 'slider_id'

                      )
                ),
                'filter' => false,
                'sortable' => false,
                'is_system' => true

            ));
        return parent::_prepareColumns();

    }

    /**
     * @return MageKeeper_Slider_Block_Adminhtml_Slider_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('slider_id');
        $this->getMassactionBlock()->setFormFieldName('slider_ids');
        $this->getMassactionBlock()
            ->addItem(
                'delete',
                array(
                    'label'    => Mage::helper('sp_simpleslider')->__('Delete'),
                    'url'      => $this->getUrl('*/*/massDelete'),
                    'confirm'  => Mage::helper('sp_simpleslider')->__('Are you sure?')
                )
            );
        return $this;
    }
    /**
     *
     * @return string
     */
    public function getRowUrl($slide)
    {
        return $this->getUrl('*/*/edit', array(
            'slider_id' => $slide->getId(),
        ));
    }
    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}