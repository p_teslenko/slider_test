<?php
class SP_SimpleSlider_Block_Adminhtml_Slider_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container

{
    const REGISTRY_KEY = 'slider_data';

    public function __construct()
    {
        parent::__construct();
        $this->_objectId    = 'slider_id';
        $this->_blockGroup  = 'simpleslider';
        $this->_controller  = 'adminhtml_slider';
        $this->_mode        = 'edit';
        $sliderId = (int)$this->getRequest()->getParam($this->_objectId);
        if (!empty($sliderId)) {
            $this->_addButton('delete', array(
                'label'     => Mage::helper('sp_simpleslider')->__('Delete'),
                'class'     => 'delete',
                'onclick'   => 'deleteConfirm(\''. Mage::helper('adminhtml')->__('Are you sure you want to do this?')
                    .'\', \'' . $this->getDeleteUrl() . '\')',
            ));
        }
        $slide = Mage::getModel('sp_simpleslider/slider')->load($sliderId);
        Mage::register(self::REGISTRY_KEY, $slide);
    }
    /**
     * @return string
     */
    public function getHeaderText()
    {
        if( Mage::registry(self::REGISTRY_KEY) && Mage::registry(self::REGISTRY_KEY)->getId() ) {
            return Mage::helper('sp_simpleslider')->__(
                "Edit slide '%s'",
                $this->escapeHtml(Mage::registry(self::REGISTRY_KEY)->getTitle())
            );
        } else {
            return Mage::helper('sp_simpleslider')->__('Add slide');
        }
    }
}

