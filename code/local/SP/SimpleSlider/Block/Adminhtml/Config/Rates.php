<?php
class SP_SimpleSlider_Block_Adminhtml_Config_Rates
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
   // protected $_itemRenderer;
    public function _prepareToRender()
    {
        $this->addColumn('from_price', array(
            'label' => Mage::helper('sp_simpleslider')->__('Rates'),
            'style' => 'width:100px',

        ));
        $this->addColumn('country_id', array(
            'label' => Mage::helper('core/data')->__('Country'),
            'style' => 'width:100px',
            'renderer' => $this->_getRenderer(),
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('sp_simpleslider')->__('Add');
    }

    protected function  _getRenderer()
    {
        if (!$this->_itemRenderer) {
            $this->_itemRenderer = $this->getLayout()->createBlock(
                'simpleslider/adminhtml_form_field_country', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_itemRenderer;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getRenderer()
                ->calcOptionHash($row->getData('country_id')),
            'selected="selected"'
        );
    }
}