<?php
class SP_SimpleSlider_Block_Adminhtml_Slider
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_slider';
        $this->_blockGroup = 'simpleslider';
        $this->_headerText = Mage::helper('sp_simpleslider')->__('Slider Grid');
        $this->_addButtonText = Mage::helper('sp_simpleslider')->__('Add Image');
        parent::__construct();
    }
}