<?php

class SP_SimpleSLider_Block_List extends Mage_Core_Block_Template
{
    /**
     * @return Varien_Data_Collection
     */
    public function getSliderImages()
    {
        return Mage::getModel('sp_simpleslider/slider')
            ->getCollection()
            ->addFieldToFilter('display_from', array(
                'from' => '2017-08-17',
                'date' => true, // specifies conversion of comparison values
            ))
            ->addFieldToFilter('display_to', array(
                'to' => '2017-12-27',
                'date' => true, // specifies conversion of comparison values
            ))
            ->addFieldToFilter('is_active', array(
                'eq' => 1
            ));
        //return Mage::getModel('sp_simpleslider/slider')->getId(6);

    }
}

