<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();

//Add short description column

$installer->getConnection()->addColumn($installer->getTable('sp_simpleslider/slider'), 'short_description', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'comment' => 'Short Description'
));

$installer->endSetup();