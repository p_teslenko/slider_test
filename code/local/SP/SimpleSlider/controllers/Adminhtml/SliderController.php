<?php
class SP_SimpleSlider_Adminhtml_SliderController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Simple Slider'));
        $this->loadLayout();
        $this->_setActiveMenu('sp_simpleslider');
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        if (!empty($data)) {
            try{
                Mage::helper('sp_simpleslider')->savePostData($data);
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('Slide hasn\'t saved')
                );
                Mage::getSingleton('adminhtml/session')->setData(SP_SimpleSlider_Block_Adminhtml_Slider_Edit::REGISTRY_KEY, $data);
                Mage::logException($e);
                return $this->_redirect('*/*/edit');
            }
        }
        return $this->_redirect('*/*/');
    }

}